/*
 * Copyright (C) 2013 Treehouse
 */
package com.rudolfschmidt.projectmanager.persistence;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@Entity
public class Topic implements Serializable {

    @Id
    @GeneratedValue
    private long id;
    private String title;
    private boolean closed;

    @NotNull
    @ManyToOne
    private Project project;
    @NotNull
    @ManyToOne
    private OneUser owner;

    @OneToMany(mappedBy = "topic")
    private List<Contribution> contributions;
    @OneToMany(mappedBy = "topic")
    private List<TopicMembering> topicMemberings;
    @OneToMany(mappedBy = "topic")
    private List<TopicLeading> topicLeadings;

    @Temporal(TemporalType.TIMESTAMP)
    private Date creationTimestamp = new Date();

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Topic other = (Topic) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public OneUser getOwner() {
        return owner;
    }

    public void setOwner(OneUser owner) {
        this.owner = owner;
    }

    public List<Contribution> getContributions() {
        return contributions;
    }

    public void setContributions(List<Contribution> contributions) {
        this.contributions = contributions;
    }

    public List<TopicMembering> getTopicMemberings() {
        return topicMemberings;
    }

    public void setTopicMemberings(List<TopicMembering> topicMemberings) {
        this.topicMemberings = topicMemberings;
    }

    public List<TopicLeading> getTopicLeadings() {
        return topicLeadings;
    }

    public void setTopicLeadings(List<TopicLeading> topicLeadings) {
        this.topicLeadings = topicLeadings;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }
}

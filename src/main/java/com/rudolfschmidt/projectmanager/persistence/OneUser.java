/*
 * Copyright (C) 2013 Treehouse
 */
package com.rudolfschmidt.projectmanager.persistence;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@Entity
public class OneUser implements Serializable {

    @Id
    @GeneratedValue
    private long id;
    private String email;
    private String firstName;
    private String lastName;
    private String hashKey;
    private boolean requestDeletion;
    private boolean activated;

    @OneToMany(mappedBy = "owner")
    private List<Project> ownedProjects;
    @OneToMany(mappedBy = "owner")
    private List<Topic> ownedTopics;
    @OneToMany(mappedBy = "owner")
    private List<Contribution> ownedContributions;

    @OneToMany(mappedBy = "projectLeader")
    private List<ProjectLeading> projectLeadings;
    @OneToMany(mappedBy = "projectMember")
    private List<ProjectMembering> projectMemberings;
    @OneToMany(mappedBy = "topicLeader")
    private List<TopicLeading> topicLeadings;
    @OneToMany(mappedBy = "topicMember")
    private List<TopicMembering> topicMemberings;

    @Temporal(TemporalType.TIMESTAMP)
    private Date signedUp = new Date();

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OneUser other = (OneUser) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getHashKey() {
        return hashKey;
    }

    public void setHashKey(String hashKey) {
        this.hashKey = hashKey;
    }

    public boolean isRequestDeletion() {
        return requestDeletion;
    }

    public void setRequestDeletion(boolean requestDeletion) {
        this.requestDeletion = requestDeletion;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public List<Project> getOwnedProjects() {
        return ownedProjects;
    }

    public void setOwnedProjects(List<Project> ownedProjects) {
        this.ownedProjects = ownedProjects;
    }

    public List<Topic> getOwnedTopics() {
        return ownedTopics;
    }

    public void setOwnedTopics(List<Topic> ownedTopics) {
        this.ownedTopics = ownedTopics;
    }

    public List<Contribution> getOwnedContributions() {
        return ownedContributions;
    }

    public void setOwnedContributions(List<Contribution> ownedContributions) {
        this.ownedContributions = ownedContributions;
    }

    public List<ProjectLeading> getProjectLeadings() {
        return projectLeadings;
    }

    public void setProjectLeadings(List<ProjectLeading> projectLeadings) {
        this.projectLeadings = projectLeadings;
    }

    public List<ProjectMembering> getProjectMemberings() {
        return projectMemberings;
    }

    public void setProjectMemberings(List<ProjectMembering> projectMemberings) {
        this.projectMemberings = projectMemberings;
    }

    public List<TopicLeading> getTopicLeadings() {
        return topicLeadings;
    }

    public void setTopicLeadings(List<TopicLeading> topicLeadings) {
        this.topicLeadings = topicLeadings;
    }

    public List<TopicMembering> getTopicMemberings() {
        return topicMemberings;
    }

    public void setTopicMemberings(List<TopicMembering> topicMemberings) {
        this.topicMemberings = topicMemberings;
    }

    public Date getSignedUp() {
        return signedUp;
    }

    public void setSignedUp(Date signedUp) {
        this.signedUp = signedUp;
    }

}

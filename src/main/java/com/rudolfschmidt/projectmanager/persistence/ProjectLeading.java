/*
 * Copyright (C) 2013 Treehouse
 */
package com.rudolfschmidt.projectmanager.persistence;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@Entity
public class ProjectLeading implements Serializable {

    @Id
    @GeneratedValue
    private long id;
    private String title;

    @NotNull
    @ManyToOne
    private OneUser projectLeader;
    @NotNull
    @ManyToOne
    private Project project;

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProjectLeading other = (ProjectLeading) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public OneUser getProjectLeader() {
        return projectLeader;
    }

    public void setProjectLeader(OneUser projectLeader) {
        this.projectLeader = projectLeader;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

}

/*
 * Copyright (C) 2013 Treehouse
 */
package com.rudolfschmidt.projectmanager.persistence;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@Entity
public class TopicLeading implements Serializable {

    @Id
    @GeneratedValue
    private long id;
    private String title;

    @NotNull
    @ManyToOne
    private OneUser topicLeader;
    @NotNull
    @ManyToOne
    private Topic topic;

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TopicLeading other = (TopicLeading) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public OneUser getTopicLeader() {
        return topicLeader;
    }

    public void setTopicLeader(OneUser topicLeader) {
        this.topicLeader = topicLeader;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

}

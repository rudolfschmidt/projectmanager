/*
 * Copyright (C) 2013 Treehouse
 */
package com.rudolfschmidt.projectmanager.persistence;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@Entity
public class Project implements Serializable {

    @Id
    @GeneratedValue
    private long id;
    private String title;
    private boolean closed;

    @NotNull
    @ManyToOne
    private OneUser owner;

    @OneToMany(mappedBy = "project")
    private List<Topic> topics;
    @OneToMany(mappedBy = "project")
    private List<ProjectMembering> projectMemberings;
    @OneToMany(mappedBy = "project")
    private List<ProjectLeading> projectLeadings;

    @Temporal(TemporalType.TIMESTAMP)
    private Date creationTimestamp = new Date();

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Project other = (Project) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public OneUser getOwner() {
        return owner;
    }

    public void setOwner(OneUser owner) {
        this.owner = owner;
    }

    public List<Topic> getTopics() {
        return topics;
    }

    public void setTopics(List<Topic> topics) {
        this.topics = topics;
    }

    public List<ProjectMembering> getProjectMemberings() {
        return projectMemberings;
    }

    public void setProjectMemberings(List<ProjectMembering> projectMemberings) {
        this.projectMemberings = projectMemberings;
    }

    public List<ProjectLeading> getProjectLeadings() {
        return projectLeadings;
    }

    public void setProjectLeadings(List<ProjectLeading> projectLeadings) {
        this.projectLeadings = projectLeadings;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

}

/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.presentation.logout;

import com.rudolfschmidt.projectmanager.business.Login;
import com.rudolfschmidt.projectmanager.persistence.OneUser;
import com.rudolfschmidt.projectmanager.presentation.contraints.Email;
import javax.enterprise.inject.Model;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@Model
public class LoginBean {

    @Email
    String email;
    String password;
    @Inject
    Login loginProcessor;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String login() {
        if (loginProcessor.loginUser(email, password)) {
            return "projects?faces-redirect=true";
        }
        final FacesContext currentInstance = FacesContext.getCurrentInstance();
        currentInstance.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login incorrect", "Try it again"));
        return null;
    }

    public String logout() {
        loginProcessor.logout();
        return "/index.jsf?faces-redirect=true";
    }

    public boolean isLoggedIn() {
        return loginProcessor.isUserLoggedIn();
    }

    public OneUser getLoggedUser() {
        return loginProcessor.getLoggedUser();
    }
}

/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.presentation.logout;

import com.rudolfschmidt.projectmanager.business.Register;
import com.rudolfschmidt.projectmanager.business.exceptions.EmailUsedException;
import com.rudolfschmidt.projectmanager.presentation.contraints.Email;
import java.io.UnsupportedEncodingException;
import javax.enterprise.inject.Model;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Size;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@Model
public class RegistrationBean {

    @Inject
    Register registrationProcessor;

    String prename;
    String surname;
    @Email
    String email;
    @Size(min = 5, max = 50)
    String password;
    @AssertTrue
    boolean conditions;

    public String getPrename() {
        return prename;
    }

    public void setPrename(String prename) {
        this.prename = prename;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isConditions() {
        return conditions;
    }

    public void setConditions(boolean conditions) {
        this.conditions = conditions;
    }

    public void action() {
        try {
            registrationProcessor.registerUser(prename, surname, email, password);
            prename = null;
            surname = null;
            email = null;
            password = null;
            conditions = false;
            final FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Congratulations your registration has been successful!",
                    "An email has been sent to you to confirm your email address.");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        } catch (EmailUsedException ex) {
            final FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, ex.getClass().getSimpleName(), ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        } catch (UnsupportedEncodingException | MessagingException ex) {
            final FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, ex.getClass().getSimpleName(), ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
    }

}

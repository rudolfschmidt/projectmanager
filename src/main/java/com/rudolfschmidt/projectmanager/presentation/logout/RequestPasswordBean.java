/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.presentation.logout;

import com.rudolfschmidt.projectmanager.business.PasswordRequestHandler;
import com.rudolfschmidt.projectmanager.presentation.contraints.Email;
import java.io.UnsupportedEncodingException;
import javax.enterprise.inject.Model;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.mail.MessagingException;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@Model
public class RequestPasswordBean {

    @Inject
    PasswordRequestHandler requestPasswordProcessor;
    @Email
    String email;

    public String action() {
        final FacesContext currentInstance = FacesContext.getCurrentInstance();
        try {
            if (requestPasswordProcessor.handlePasswordRequest(email)) {
                currentInstance.addMessage(null, new FacesMessage(
                        "Good, now go check your mailbox!",
                        "Please check your mailbox. The email delivery may take a few moments. If you don't receive this email, please check your Junk or Spam folder "));
            } else {
                currentInstance.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "User not found!",
                        "Try another email address"));
            }
        } catch (UnsupportedEncodingException | MessagingException ex) {
            final FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, ex.getClass().getSimpleName(), ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
        return null;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}

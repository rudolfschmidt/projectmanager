/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.presentation;

import com.rudolfschmidt.projectmanager.business.HashKey;
import com.rudolfschmidt.projectmanager.business.NewUserPasswordSetter;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.Size;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@Named
@SessionScoped
public class NewPasswordBean implements Serializable {

    @Inject
    private HashKey hashKeyValidator;
    @Inject
    private NewUserPasswordSetter newUserPasswordProcessor;
    @Size(min = 5, max = 50)
    private String password;
    @Size(min = 5, max = 50)
    private String confirmationPassword;
    private String verificationKey;
    private boolean keyValid;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmationPassword() {
        return confirmationPassword;
    }

    public void setConfirmationPassword(String confirmationPassword) {
        this.confirmationPassword = confirmationPassword;
    }

    public String getVerificationKey() {
        return verificationKey;
    }

    public void setVerificationKey(String verificationKey) {
        this.verificationKey = verificationKey;
    }

    public boolean isKeyValid() {
        return keyValid;
    }

    public String action() {
        final FacesContext currentInstance = FacesContext.getCurrentInstance();
        if (newUserPasswordProcessor.setNewUserPassword(verificationKey, password)) {
            final FacesMessage facesMessage = new FacesMessage("New Password is accepted", "You can use your new password now");
            currentInstance.addMessage(null, facesMessage);
        } else {
            final FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Some error happen", "user not found");
            currentInstance.addMessage(null, facesMessage);
        }
        return null;
    }

    public void validateKey() {
        if (hashKeyValidator.validateUserHashKey(verificationKey)) {
            keyValid = true;
        } else {
            final FacesContext currentInstance = FacesContext.getCurrentInstance();
            final FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_FATAL,
                    "Key is invalid",
                    "You cant change your password.");
            currentInstance.addMessage(null, facesMessage);
            keyValid = false;
        }
    }
}

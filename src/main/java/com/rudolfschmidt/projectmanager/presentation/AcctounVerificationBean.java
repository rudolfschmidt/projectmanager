/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.presentation;

import com.rudolfschmidt.projectmanager.business.UserAccountActivator;
import javax.enterprise.inject.Model;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@Model
public class AcctounVerificationBean {

    @Inject
    private UserAccountActivator userAccountActivator;
    private String verificationKey;

    public String getVerificationKey() {
        return verificationKey;
    }

    public void setVerificationKey(String verificationKey) {
        this.verificationKey = verificationKey;
    }

    public void validateKey() {
        final FacesContext currentInstance = FacesContext.getCurrentInstance();
        if (userAccountActivator.activateUserAccount(verificationKey)) {
            currentInstance.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Thank you! ",
                    "Your email has been verified."));
        } else {
            currentInstance.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Your activation key is invalid",
                    "Your email could not be verified"));
        }
    }
}

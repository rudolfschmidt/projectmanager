/*
 * Copyright (C) 2013 Treehouse
 */
package com.rudolfschmidt.projectmanager.presentation;

import com.rudolfschmidt.projectmanager.business.mailer.FeedbackMailer;
import javax.enterprise.inject.Model;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.mail.MessagingException;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@Model
public class FeedbackBean
{
    @Inject
    private FeedbackMailer feedbackProcessor;
    private int rating;
    private String message;

    public void submit()
    {
        final FacesContext currentInstance = FacesContext.getCurrentInstance();
        try {
            feedbackProcessor.sendFeedback(message, rating);
            final FacesMessage facesMessage = new FacesMessage("Your message was sent!", "we will answer as soon as possible!");
            currentInstance.addMessage(null, facesMessage);
        } catch (MessagingException ex) {
            currentInstance.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, ex.getMessage(), ex.getMessage()));
        }
    }

    public int getRating()
    {
        return rating;
    }

    public void setRating(int rating)
    {
        this.rating = rating;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

}

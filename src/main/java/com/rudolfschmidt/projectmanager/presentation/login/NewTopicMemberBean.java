/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.presentation.login;

import com.rudolfschmidt.projectmanager.business.core.TopicProcessor;
import com.rudolfschmidt.projectmanager.business.exceptions.ForbiddenOperationException;
import com.rudolfschmidt.projectmanager.business.exceptions.InvalidProjectMemberException;
import com.rudolfschmidt.projectmanager.persistence.OneUser;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@Named
@SessionScoped
public class NewTopicMemberBean implements Serializable {

    @Inject
    TopicProcessor topicProcessor;
    @Inject
    TopicBean topicBean;
    OneUser[] selectedMembers;

    public void add() {
        for (OneUser user : selectedMembers) {
            try {
                topicProcessor.addMember(topicBean.getTopic(), user);
            } catch (InvalidProjectMemberException ex) {
                final FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, ex.getClass().getSimpleName(), ex.getMessage());
                FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            } catch (ForbiddenOperationException ex) {
                final FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, ex.getClass().getSimpleName(), ex.getMessage());
                FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            }
        }
        selectedMembers = null;
    }

    public List<OneUser> getMembers() {
        return topicProcessor.getAvailableMembers(topicBean.getTopic());
    }

    public OneUser[] getSelectedMembers() {
        return selectedMembers;
    }

    public void setSelectedMembers(OneUser[] selectedMembers) {
        this.selectedMembers = selectedMembers;
    }
}

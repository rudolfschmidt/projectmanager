/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.presentation.login;

import com.rudolfschmidt.projectmanager.business.core.ProjectProcessor;
import com.rudolfschmidt.projectmanager.business.exceptions.ForbiddenOperationException;
import com.rudolfschmidt.projectmanager.persistence.OneUser;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@Named
@SessionScoped
public class ProjectLeaderBean implements Serializable {

    @Inject
    ProjectProcessor projectProcessor;
    @Inject
    ProjectBean projectBean;
    OneUser[] selectedLeaders;

    public void remove() {
        for (OneUser user : selectedLeaders) {
            try {
                projectProcessor.removeLeader(projectBean.getProject(), user);
            } catch (ForbiddenOperationException ex) {
                final FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, ex.getClass().getSimpleName(), ex.getMessage());
                FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            }
        }
        selectedLeaders = null;
    }

    public List<OneUser> getLeaders() {
        return projectProcessor.getCurrentLeaders(projectBean.getProject());
    }

    public OneUser[] getSelectedLeaders() {
        return selectedLeaders;
    }

    public void setSelectedLeaders(OneUser[] selectedLeaders) {
        this.selectedLeaders = selectedLeaders;
    }

}

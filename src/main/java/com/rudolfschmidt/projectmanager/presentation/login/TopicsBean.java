/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.presentation.login;

import com.rudolfschmidt.projectmanager.business.core.TopicProcessor;
import com.rudolfschmidt.projectmanager.business.exceptions.ForbiddenOperationException;
import com.rudolfschmidt.projectmanager.persistence.Topic;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@Named
@SessionScoped
public class TopicsBean implements Serializable {

    @Inject
    ProjectBean projectBean;
    @Inject
    TopicProcessor topicProcessor;
    Topic[] selectedTopics;

    public void remove() {
        for (Topic topic : selectedTopics) {
            try {
                topicProcessor.remove(topic);
            } catch (ForbiddenOperationException ex) {
                final FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, ex.getClass().getSimpleName(), ex.getMessage());
                FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            }
        }
        selectedTopics = null;
    }

    public String edit() {
        final long id = selectedTopics[0].getId();
        selectedTopics = null;
        return "topic?faces-redirect=true&id=" + id;
    }

    public List<Topic> getTopics() {
        return topicProcessor.getTopics(projectBean.getProjectId());
    }

    public List<Topic> getAllTopics() {
        return topicProcessor.getTopics();
    }

    public Topic[] getSelectedTopics() {
        return selectedTopics;
    }

    public void setSelectedTopics(Topic[] selectedTopics) {
        this.selectedTopics = selectedTopics;
    }

}

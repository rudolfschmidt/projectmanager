/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.presentation.login;

import com.rudolfschmidt.projectmanager.business.core.ProjectProcessor;
import com.rudolfschmidt.projectmanager.persistence.Project;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@Named
@SessionScoped
public class ProjectBean implements Serializable {

    @Inject
    ProjectProcessor projectProcessor;
    long projectId;
    Project project;
    boolean hidden;

    public void preRenderView() {
        final Project found = projectProcessor.getProject(projectId);
        if (found == null) {
            hidden = true;
        } else {
            project = found;
            hidden = false;
        }
    }

    public String save() {
        projectProcessor.update(project);
        final FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Changes are saved", null);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        return null;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public Project getProject() {
        return project;
    }

    public boolean isHidden() {
        return hidden;
    }

}

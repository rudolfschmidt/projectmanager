/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.presentation.login;

import com.rudolfschmidt.projectmanager.business.core.ContributionProcessor;
import com.rudolfschmidt.projectmanager.business.exceptions.ForbiddenOperationException;
import com.rudolfschmidt.projectmanager.persistence.Contribution;
import java.util.List;
import javax.enterprise.inject.Model;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@Model
public class ContributionsBean {

    @Inject
    ContributionProcessor contributionProcessor;
    @Inject
    TopicBean topicBean;
    String content;

    public void create() {
        try {
            contributionProcessor.create(topicBean.getTopic(), content);
        } catch (ForbiddenOperationException ex) {
            final FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_WARN, ex.getMessage(), ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, facesMessage1);
        }
        content = null;
    }

    public List<Contribution> getContributions() {
        return contributionProcessor.getContributions(topicBean.getTopic());
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}

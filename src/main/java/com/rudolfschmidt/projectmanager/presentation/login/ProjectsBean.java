/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.presentation.login;

import com.rudolfschmidt.projectmanager.business.core.ProjectProcessor;
import com.rudolfschmidt.projectmanager.business.exceptions.ForbiddenOperationException;
import com.rudolfschmidt.projectmanager.persistence.Project;
import java.io.Serializable;
import java.util.Collection;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@Named
@SessionScoped
public class ProjectsBean implements Serializable {

    @Inject
    ProjectProcessor projectProcessor;
    Project[] selectedProjects;

    public void remove() {
        for (Project project : selectedProjects) {
            try {
                projectProcessor.remove(project);
            } catch (ForbiddenOperationException ex) {
                final FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_WARN, ex.getClass().getSimpleName(), ex.getMessage());
                FacesContext.getCurrentInstance().addMessage(null, facesMessage1);
            }
        }
        selectedProjects = null;
    }

    public String edit() {
        final long id = selectedProjects[0].getId();
        selectedProjects = null;
        return "project?faces-redirect=true&id=" + id;
    }

    public Collection<Project> getProjects() {
        return projectProcessor.getProjects();
    }

    public Project[] getSelectedProjects() {
        return selectedProjects;
    }

    public void setSelectedProjects(Project[] selectedProjects) {
        this.selectedProjects = selectedProjects;
    }

}

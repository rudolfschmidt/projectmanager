/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.presentation.login;

import com.rudolfschmidt.projectmanager.business.core.ProjectProcessor;
import com.rudolfschmidt.projectmanager.business.core.TopicProcessor;
import com.rudolfschmidt.projectmanager.business.exceptions.ForbiddenOperationException;
import com.rudolfschmidt.projectmanager.business.exceptions.InvalidTitleException;
import com.rudolfschmidt.projectmanager.persistence.Project;
import java.util.List;
import javax.enterprise.inject.Model;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@Model
public class NewTopicBean {

    @Inject
    ProjectProcessor projectProcessor;
    @Inject
    TopicProcessor topicProcessor;
    String title;
    long projectId;

    public void create() {
        try {
            topicProcessor.create(projectId, title);
        } catch (InvalidTitleException ex) {
            final FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, ex.getClass().getSimpleName(), ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        } catch (ForbiddenOperationException ex) {
            final FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, ex.getClass().getSimpleName(), ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
        title = null;
    }

    public List<Project> getProjects() {
        return projectProcessor.getProjects();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

}

/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.presentation.login;

import com.rudolfschmidt.projectmanager.business.core.ProjectProcessor;
import com.rudolfschmidt.projectmanager.business.exceptions.InvalidTitleException;
import javax.enterprise.inject.Model;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@Model
public class NewProjectBean {

    @Inject
    ProjectProcessor projectProcessor;

    String title;

    public void create() {
        try {
            projectProcessor.create(title);
        } catch (InvalidTitleException ex) {
            final FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_WARN, ex.getClass().getSimpleName(), ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, facesMessage1);
        }
        title = null;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}

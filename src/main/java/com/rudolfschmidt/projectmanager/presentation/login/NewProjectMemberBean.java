/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.presentation.login;

import com.rudolfschmidt.projectmanager.business.core.ProjectProcessor;
import com.rudolfschmidt.projectmanager.business.exceptions.ForbiddenOperationException;
import com.rudolfschmidt.projectmanager.persistence.OneUser;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@Named
@SessionScoped
public class NewProjectMemberBean implements Serializable {

    @Inject
    ProjectProcessor projectProcessor;
    @Inject
    ProjectBean projectBean;
    OneUser[] selectedMembers;

    public void add() {
        for (OneUser user : selectedMembers) {
            try {
                projectProcessor.addMember(projectBean.getProject(), user);
            } catch (ForbiddenOperationException ex) {
                final FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, ex.getClass().getSimpleName(), ex.getMessage());
                FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            }
        }
        selectedMembers = null;
    }

    public List<OneUser> getMembers() {
        return projectProcessor.getAvailableMembers(projectBean.getProject());
    }

    public OneUser[] getSelectedMembers() {
        return selectedMembers;
    }

    public void setSelectedMembers(OneUser[] selectedMembers) {
        this.selectedMembers = selectedMembers;
    }

}

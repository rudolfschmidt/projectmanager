/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.presentation.login;

import com.rudolfschmidt.projectmanager.business.core.TopicProcessor;
import com.rudolfschmidt.projectmanager.persistence.Topic;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@Named
@SessionScoped
public class TopicBean implements Serializable {

    @Inject
    TopicProcessor topicProcessor;
    long topicId;
    Topic topic;
    boolean hidden;

    public void preRenderView() {
        final Topic found = topicProcessor.getTopic(topicId);
        if (found == null) {
            hidden = true;
        } else {
            topic = found;
            hidden = false;
        }
    }

    public String save() {
        topicProcessor.update(topic);
        final FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Changes are saved", null);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        return null;
    }

    public long getTopicId() {
        return topicId;
    }

    public void setTopicId(long topicId) {
        this.topicId = topicId;
    }

    public Topic getTopic() {
        return topic;
    }

    public boolean isHidden() {
        return hidden;
    }

}

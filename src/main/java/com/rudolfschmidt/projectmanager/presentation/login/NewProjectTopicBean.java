/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.presentation.login;

import com.rudolfschmidt.projectmanager.business.core.TopicProcessor;
import com.rudolfschmidt.projectmanager.business.exceptions.ForbiddenOperationException;
import com.rudolfschmidt.projectmanager.business.exceptions.InvalidTitleException;
import javax.enterprise.inject.Model;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@Model
public class NewProjectTopicBean {

    @Inject
    ProjectBean projectBean;
    @Inject
    TopicProcessor topicProcessor;
    String title;

    public void create() {
        try {
            topicProcessor.create(projectBean.getProject(), title);
        } catch (InvalidTitleException ex) {
            final FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, ex.getClass().getSimpleName(), ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        } catch (ForbiddenOperationException ex) {
            final FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, ex.getClass().getSimpleName(), ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
        title = null;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}

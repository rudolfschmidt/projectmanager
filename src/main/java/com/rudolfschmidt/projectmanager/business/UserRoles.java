/*
 * Copyright (C) 2013 Treehouse
 */
package com.rudolfschmidt.projectmanager.business;

import com.rudolfschmidt.projectmanager.persistence.Contribution;
import com.rudolfschmidt.projectmanager.persistence.OneUser;
import com.rudolfschmidt.projectmanager.persistence.Project;
import com.rudolfschmidt.projectmanager.persistence.ProjectLeading;
import com.rudolfschmidt.projectmanager.persistence.ProjectMembering;
import com.rudolfschmidt.projectmanager.persistence.Topic;
import com.rudolfschmidt.projectmanager.persistence.TopicLeading;
import com.rudolfschmidt.projectmanager.persistence.TopicMembering;
import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@RequestScoped
public class UserRoles {

    @PersistenceContext
    EntityManager entityManager;

    public boolean isProjectOwner(Project project, OneUser user) {
        return project.getOwner().equals(user);
    }

    public boolean isProjectLeader(Project project, OneUser user) {
        final OneUser found = entityManager.find(OneUser.class, user.getId());
        for (ProjectLeading projectLeading : found.getProjectLeadings()) {
            if (projectLeading.getProject().equals(project)) {
                return true;
            }
        }
        return false;
    }

    public boolean isProjectMember(Project project, OneUser user) {
        final OneUser find = entityManager.find(OneUser.class, user.getId());
        for (ProjectMembering projectMembering : find.getProjectMemberings()) {
            if (projectMembering.getProject().equals(project)) {
                return true;
            }
        }
        return false;
    }

    public boolean isTopicOwner(Topic topic, OneUser user) {
        return topic.getOwner().equals(user);
    }

    public boolean isTopicLeader(Topic topic, OneUser user) {
        final OneUser find = entityManager.find(OneUser.class, user.getId());
        for (TopicLeading topicLeading : find.getTopicLeadings()) {
            if (topicLeading.getTopic().equals(topic)) {
                return true;
            }
        }
        return false;
    }

    public boolean isTopicMember(Topic topic, OneUser user) {
        final OneUser find = entityManager.find(OneUser.class, user.getId());
        for (TopicMembering topicMembering : find.getTopicMemberings()) {
            if (topicMembering.getTopic().equals(topic)) {
                return true;
            }
        }
        return false;
    }

    public boolean isContributionOwner(Contribution contribution, OneUser user) {
        return contribution.getOwner().equals(user);
    }
}

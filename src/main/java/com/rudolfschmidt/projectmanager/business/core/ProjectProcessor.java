/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.business.core;

import com.rudolfschmidt.projectmanager.business.UserRoles;
import com.rudolfschmidt.projectmanager.business.exceptions.ForbiddenOperationException;
import com.rudolfschmidt.projectmanager.business.exceptions.InvalidTitleException;
import com.rudolfschmidt.projectmanager.business.qualifiers.LoggedUser;
import com.rudolfschmidt.projectmanager.persistence.Contribution;
import com.rudolfschmidt.projectmanager.persistence.OneUser;
import com.rudolfschmidt.projectmanager.persistence.Project;
import com.rudolfschmidt.projectmanager.persistence.ProjectLeading;
import com.rudolfschmidt.projectmanager.persistence.ProjectMembering;
import com.rudolfschmidt.projectmanager.persistence.Topic;
import com.rudolfschmidt.projectmanager.persistence.TopicLeading;
import com.rudolfschmidt.projectmanager.persistence.TopicMembering;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.transaction.Transactional;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@RequestScoped
public class ProjectProcessor {

    @PersistenceContext
    EntityManager entityManager;
    @Inject
    @LoggedUser
    OneUser loggedUser;
    @Inject
    UserRoles userRightsProcessor;

    @Transactional
    public long create(String newProjectTitle)
            throws InvalidTitleException {
        loggedUser = entityManager.find(OneUser.class, loggedUser.getId());
        for (Project project : loggedUser.getOwnedProjects()) {
            if (project.getTitle().equals(newProjectTitle)) {
                throw new InvalidTitleException();
            }
        }
        final Project project = new Project();
        project.setTitle(newProjectTitle);
        project.setOwner(loggedUser);
        final ProjectLeading projectLeading = new ProjectLeading();
        projectLeading.setProjectLeader(loggedUser);
        projectLeading.setProject(project);
        projectLeading.setTitle(newProjectTitle);
        loggedUser.getOwnedProjects().add(project);
        loggedUser.getProjectLeadings().add(projectLeading);
        entityManager.persist(project);
        entityManager.persist(projectLeading);
        entityManager.merge(project).getProjectLeadings().add(projectLeading);
        return project.getId();
    }

    @Transactional
    public void remove(Project project) throws ForbiddenOperationException {
        if (!userRightsProcessor.isProjectOwner(project, loggedUser)) {
            throw new ForbiddenOperationException();
        }
        loggedUser = entityManager.find(OneUser.class, loggedUser.getId());
        final Iterator<Contribution> contributions = loggedUser.getOwnedContributions().iterator();
        while (contributions.hasNext()) {
            if (contributions.next().getTopic().getProject().equals(project)) {
                contributions.remove();
            }
        }
        final Iterator<TopicLeading> topicLeadings = loggedUser.getTopicLeadings().iterator();
        while (topicLeadings.hasNext()) {
            if (topicLeadings.next().getTopic().getProject().equals(project)) {
                topicLeadings.remove();
            }
        }
        final Iterator<TopicMembering> topicMemberings = loggedUser.getTopicMemberings().iterator();
        while (topicMemberings.hasNext()) {
            if (topicMemberings.next().getTopic().getProject().equals(project)) {
                topicMemberings.remove();
            }
        }
        final Iterator<Topic> topics = loggedUser.getOwnedTopics().iterator();
        while (topics.hasNext()) {
            if (topics.next().getProject().equals(project)) {
                topics.remove();
            }
        }
        final Iterator<ProjectLeading> projectLeadings = loggedUser.getProjectLeadings().iterator();
        while (projectLeadings.hasNext()) {
            if (projectLeadings.next().getProject().equals(project)) {
                projectLeadings.remove();
            }
        }

        final Iterator<ProjectMembering> projectMemberings = loggedUser.getProjectMemberings().iterator();
        while (projectMemberings.hasNext()) {
            if (projectMemberings.next().getProject().equals(project)) {
                projectMemberings.remove();
            }
        }
        final Iterator<Project> projects = loggedUser.getOwnedProjects().iterator();
        while (projects.hasNext()) {
            if (projects.next().equals(project)) {
                projects.remove();
            }
        }
        project = entityManager.find(Project.class, project.getId());
        for (Topic topic : project.getTopics()) {
            for (Contribution contribution : topic.getContributions()) {
                entityManager.remove(contribution);
            }
            for (TopicLeading topicLeading : topic.getTopicLeadings()) {
                entityManager.remove(topicLeading);
            }
            for (TopicMembering topicMembering : topic.getTopicMemberings()) {
                entityManager.remove(topicMembering);
            }
            entityManager.remove(topic);
        }
        for (ProjectLeading projectLeading : project.getProjectLeadings()) {
            entityManager.remove(projectLeading);
        }
        for (ProjectMembering projectMembering : project.getProjectMemberings()) {
            entityManager.remove(projectMembering);

        }
        entityManager.remove(project);
    }

    @Transactional
    public void rename(Project project, String newTitle) {
        loggedUser = entityManager.merge(loggedUser);
        if (userRightsProcessor.isProjectOwner(project, loggedUser)) {
            project.setTitle(newTitle);
        }
        for (ProjectLeading projectLeading : loggedUser.getProjectLeadings()) {
            if (projectLeading.getProject().equals(project)) {
                projectLeading.setTitle(newTitle);
            }
        }
        for (ProjectMembering projectMembering : loggedUser.getProjectMemberings()) {
            if (projectMembering.getProject().equals(project)) {
                projectMembering.setTitle(newTitle);
            }
        }
    }

    public void close(Project project)
            throws ForbiddenOperationException {
        if (!userRightsProcessor.isProjectOwner(project, loggedUser)) {
            if (!userRightsProcessor.isProjectLeader(project, loggedUser)) {
                throw new ForbiddenOperationException();
            }
        }
        project = entityManager.merge(project);
        project.setClosed(true);
    }

    @Transactional
    public void open(Project project)
            throws ForbiddenOperationException {
        if (!userRightsProcessor.isProjectOwner(project, loggedUser)) {
            if (!userRightsProcessor.isProjectLeader(project, loggedUser)) {
                throw new ForbiddenOperationException();
            }
        }
        project = entityManager.merge(project);
        project.setClosed(false);
    }

    @Transactional
    public void addLeader(Project project, OneUser user)
            throws ForbiddenOperationException {
        if (!userRightsProcessor.isProjectOwner(project, loggedUser)) {
            throw new ForbiddenOperationException();
        }
        project = entityManager.find(Project.class, project.getId());
        user = entityManager.find(OneUser.class, user.getId());
        final ProjectLeading projectLeading = new ProjectLeading();
        projectLeading.setProject(project);
        projectLeading.setProjectLeader(user);
        projectLeading.setTitle(project.getTitle());
        project.getProjectLeadings().add(projectLeading);
        user.getProjectLeadings().add(projectLeading);
        entityManager.persist(projectLeading);
    }

    @Transactional
    public void removeLeader(Project project, OneUser user)
            throws ForbiddenOperationException {
        if (!userRightsProcessor.isProjectOwner(project, loggedUser)) {
            throw new ForbiddenOperationException();
        }
        user = entityManager.find(OneUser.class, user.getId());
        final Iterator<ProjectLeading> userIt = user.getProjectLeadings().iterator();
        while (userIt.hasNext()) {
            final ProjectLeading current = userIt.next();
            if (current.getProject().equals(project)) {
                userIt.remove();
            }
        }
        project = entityManager.find(Project.class, project.getId());
        final Iterator<ProjectLeading> projectIt = project.getProjectLeadings().iterator();
        while (projectIt.hasNext()) {
            final ProjectLeading current = projectIt.next();
            if (current.getProjectLeader().equals(user)) {
                projectIt.remove();
                entityManager.remove(current);
            }
        }
        if (project.getProjectLeadings().isEmpty()) {
            addLeader(project, loggedUser);
        }
    }

    @Transactional
    public void addMember(Project project, OneUser user)
            throws ForbiddenOperationException {
        if (!userRightsProcessor.isProjectOwner(project, loggedUser)) {
            if (!userRightsProcessor.isProjectLeader(project, loggedUser)) {
                throw new ForbiddenOperationException();
            }
        }
        project = entityManager.find(Project.class, project.getId());
        user = entityManager.find(OneUser.class, user.getId());
        final ProjectMembering projectMembering = new ProjectMembering();
        projectMembering.setProject(project);
        projectMembering.setProjectMember(user);
        projectMembering.setTitle(project.getTitle());
        project.getProjectMemberings().add(projectMembering);
        user.getProjectMemberings().add(projectMembering);
        entityManager.persist(projectMembering);

    }

    @Transactional
    public void removeMember(Project project, OneUser user)
            throws ForbiddenOperationException {
        if (!userRightsProcessor.isProjectOwner(project, loggedUser)) {
            if (!userRightsProcessor.isProjectLeader(project, loggedUser)) {
                throw new ForbiddenOperationException();
            }
        }
        project = entityManager.find(Project.class, project.getId());
        final Iterator<ProjectMembering> projectIt = project.getProjectMemberings().iterator();
        while (projectIt.hasNext()) {
            final ProjectMembering current = projectIt.next();
            if (current.getProjectMember().equals(user)) {
                projectIt.remove();
            }
        }
        user = entityManager.find(OneUser.class, user.getId());
        final Iterator<ProjectMembering> userIt = user.getProjectMemberings().iterator();
        while (userIt.hasNext()) {
            final ProjectMembering current = userIt.next();
            if (current.getProject().equals(project)) {
                entityManager.remove(current);
                userIt.remove();
            }
        }
    }

    public List<Project> getProjects() {
        final Set<Project> projects = new HashSet<>();
        loggedUser = entityManager.find(OneUser.class, loggedUser.getId());
        projects.addAll(loggedUser.getOwnedProjects());
        for (ProjectLeading projectLeading : loggedUser.getProjectLeadings()) {
            projects.add(projectLeading.getProject());
        }
        for (ProjectMembering projectMembering : loggedUser.getProjectMemberings()) {
            projects.add(projectMembering.getProject());
        }
        return new ArrayList<>(projects);
    }

    public Project getProject(long id) {
        return entityManager.find(Project.class, id);
    }

    @Transactional
    public void update(Project project) {
        entityManager.merge(project);
    }

    public List<OneUser> getAvailableLeaders(Project project) {
        final Set<OneUser> ret = new HashSet<>();
        final List<ProjectLeading> projectLeadings = project.getProjectLeadings();
        final CriteriaQuery<OneUser> c = entityManager.getCriteriaBuilder().createQuery(OneUser.class);
        final List<OneUser> resultList = entityManager.createQuery(c.select(c.from(OneUser.class))).getResultList();
        for (OneUser oneUser : resultList) {
            boolean found = false;
            for (ProjectLeading projectLeading : projectLeadings) {
                if (oneUser.equals(projectLeading.getProjectLeader())) {
                    found = true;
                }
            }
            if (!found) {
                ret.add(oneUser);
            }
        }
        return new ArrayList<>(ret);
    }

    public List<OneUser> getCurrentLeaders(Project project) {
        project = entityManager.find(Project.class, project.getId());
        final List<OneUser> ret = new ArrayList<>();
        for (ProjectLeading projectLeading : project.getProjectLeadings()) {
            ret.add(projectLeading.getProjectLeader());
        }
        return ret;

    }

    public List<OneUser> getAvailableMembers(Project project) {
        final Set<OneUser> ret = new HashSet<>();
        final List<ProjectMembering> projectMemberings = project.getProjectMemberings();
        final CriteriaQuery<OneUser> c = entityManager.getCriteriaBuilder().createQuery(OneUser.class);
        final List<OneUser> resultList = entityManager.createQuery(c.select(c.from(OneUser.class))).getResultList();
        for (OneUser oneUser : resultList) {
            boolean found = false;
            for (ProjectMembering projectMembering : projectMemberings) {
                if (oneUser.equals(projectMembering.getProjectMember())) {
                    found = true;
                }
            }
            if (!found) {
                ret.add(oneUser);
            }
        }
        return new ArrayList<>(ret);
    }

    public List<OneUser> getCurrentMembers(Project project) {
        project = entityManager.find(Project.class, project.getId());
        final List<OneUser> ret = new ArrayList<>();
        for (ProjectMembering projectMembering : project.getProjectMemberings()) {
            ret.add(projectMembering.getProjectMember());
        }
        return ret;
    }

}

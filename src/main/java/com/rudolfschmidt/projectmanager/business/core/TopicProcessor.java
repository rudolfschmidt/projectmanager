/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.business.core;

import com.rudolfschmidt.projectmanager.business.UserRoles;
import com.rudolfschmidt.projectmanager.business.exceptions.ForbiddenOperationException;
import com.rudolfschmidt.projectmanager.business.exceptions.InvalidProjectMemberException;
import com.rudolfschmidt.projectmanager.business.exceptions.InvalidTitleException;
import com.rudolfschmidt.projectmanager.business.qualifiers.LoggedUser;
import com.rudolfschmidt.projectmanager.persistence.Contribution;
import com.rudolfschmidt.projectmanager.persistence.OneUser;
import com.rudolfschmidt.projectmanager.persistence.Project;
import com.rudolfschmidt.projectmanager.persistence.Topic;
import com.rudolfschmidt.projectmanager.persistence.TopicLeading;
import com.rudolfschmidt.projectmanager.persistence.TopicMembering;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.transaction.Transactional;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@RequestScoped
public class TopicProcessor {

    @PersistenceContext
    EntityManager entityManager;
    @Inject
    @LoggedUser
    OneUser loggedUser;
    @Inject
    UserRoles userRightsProcessor;

    @Transactional
    public void create(long projectId, String topicTitle)
            throws InvalidTitleException, ForbiddenOperationException {
        final Project project = entityManager.find(Project.class, projectId);
        create(project, topicTitle);
    }

    @Transactional
    public void create(Project project, String topicTitle)
            throws InvalidTitleException, ForbiddenOperationException {
        project = entityManager.find(Project.class, project.getId());
        if (!userRightsProcessor.isProjectOwner(project, loggedUser)) {
            if (!userRightsProcessor.isProjectLeader(project, loggedUser)) {
                if (!userRightsProcessor.isProjectMember(project, loggedUser)) {
                    throw new ForbiddenOperationException();
                }
            }
        }
        loggedUser = entityManager.find(OneUser.class, loggedUser.getId());
        for (Topic topic : loggedUser.getOwnedTopics()) {
            if (topic.getProject().equals(project) && topic.getTitle().equals(topicTitle)) {
                throw new InvalidTitleException();
            }
        }
        final Topic topic = new Topic();
        topic.setOwner(loggedUser);
        topic.setProject(project);
        topic.setTitle(topicTitle);
        final TopicLeading topicLeading = new TopicLeading();
        topicLeading.setTitle(topicTitle);
        topicLeading.setTopic(topic);
        topicLeading.setTopicLeader(loggedUser);
        loggedUser.getOwnedTopics().add(topic);
        loggedUser.getTopicLeadings().add(topicLeading);
        project.getTopics().add(topic);
        entityManager.persist(topic);
        entityManager.persist(topicLeading);
        entityManager.merge(topic).getTopicLeadings().add(topicLeading);
    }

    @Transactional
    public void remove(Topic topic)
            throws ForbiddenOperationException {
        topic = entityManager.find(Topic.class, topic.getId());
        final Project project = topic.getProject();
        if (!userRightsProcessor.isProjectOwner(project, loggedUser)) {
            if (!userRightsProcessor.isProjectLeader(project, loggedUser)) {
                throw new ForbiddenOperationException();
            }
        }
        loggedUser = entityManager.find(OneUser.class, loggedUser.getId());
        final Iterator<Contribution> contributions = loggedUser.getOwnedContributions().iterator();
        while (contributions.hasNext()) {
            final Contribution current = contributions.next();
            if (current.getTopic().equals(topic)) {
                contributions.remove();
            }
        }
        final Iterator<TopicLeading> topicLeadings = loggedUser.getTopicLeadings().iterator();
        while (topicLeadings.hasNext()) {
            final TopicLeading current = topicLeadings.next();
            if (current.getTopic().equals(topic)) {
                topicLeadings.remove();
            }
        }
        final Iterator<TopicMembering> topicMemberings = loggedUser.getTopicMemberings().iterator();
        while (topicMemberings.hasNext()) {
            final TopicMembering current = topicMemberings.next();
            if (current.getTopic().equals(topic)) {
                topicMemberings.remove();
            }
        }
        final Iterator<Topic> ownTopics = loggedUser.getOwnedTopics().iterator();
        while (ownTopics.hasNext()) {
            final Topic current = ownTopics.next();
            if (current.equals(topic)) {
                ownTopics.remove();
            }
        }
        final Iterator<Topic> topicIterator = project.getTopics().iterator();
        while (topicIterator.hasNext()) {
            final Topic current = topicIterator.next();
            if (current.equals(topic)) {
                topicIterator.remove();
            }
        }
        for (Contribution contribution : topic.getContributions()) {
            entityManager.remove(contribution);
        }
        for (TopicLeading topicLeading : topic.getTopicLeadings()) {
            entityManager.remove(topicLeading);
        }
        for (TopicMembering topicMembering : topic.getTopicMemberings()) {
            entityManager.remove(topicMembering);
        }
        entityManager.remove(topic);
    }

    @Transactional
    public void rename(Topic topic, String newTitle) {
        topic = entityManager.find(Topic.class, topic.getId());
        loggedUser = entityManager.merge(loggedUser);
        if (topic.getOwner().getId() == loggedUser.getId()) {
            topic.setTitle(newTitle);
        }
        for (TopicLeading topicLeading : loggedUser.getTopicLeadings()) {
            if (topicLeading.getTopic().equals(topic)) {
                topicLeading.setTitle(newTitle);
            }
        }
        for (TopicMembering topicMembering : loggedUser.getTopicMemberings()) {
            if (topicMembering.getTopic().equals(topic)) {
                topicMembering.setTitle(newTitle);
            }
        }
    }

    @Transactional
    public void close(Topic topic) throws ForbiddenOperationException {
        topic = entityManager.find(Topic.class, topic.getId());
        loggedUser = entityManager.find(OneUser.class, loggedUser.getId());
        final Project project = topic.getProject();
        if (!userRightsProcessor.isProjectOwner(project, loggedUser)) {
            if (!userRightsProcessor.isProjectLeader(project, loggedUser)) {
                if (!userRightsProcessor.isTopicOwner(topic, loggedUser)) {
                    throw new ForbiddenOperationException();
                }
                for (Contribution contribution : topic.getContributions()) {
                    if (!contribution.getOwner().equals(loggedUser)) {
                        throw new ForbiddenOperationException();
                    }
                }
            }
        }
        topic = entityManager.merge(topic);
        topic.setClosed(true);
    }

    @Transactional
    public void open(Topic topic) throws ForbiddenOperationException {
        final Project project = topic.getProject();
        if (!userRightsProcessor.isProjectOwner(project, loggedUser)) {
            if (!userRightsProcessor.isProjectLeader(project, loggedUser)) {
                throw new ForbiddenOperationException();
            }
        }
        topic = entityManager.merge(topic);
        topic.setClosed(false);
    }

    @Transactional
    public void addLeader(Topic topic, OneUser user)
            throws InvalidProjectMemberException, ForbiddenOperationException {
        final Project project = topic.getProject();
        if (!userRightsProcessor.isProjectOwner(project, loggedUser)) {
            if (!userRightsProcessor.isProjectLeader(project, loggedUser)) {
                throw new ForbiddenOperationException();
            }
        }
        if (!userRightsProcessor.isProjectOwner(project, user)) {
            if (!userRightsProcessor.isProjectMember(project, user)) {
                throw new InvalidProjectMemberException();
            }
        }
        topic = entityManager.find(Topic.class, topic.getId());
        user = entityManager.find(OneUser.class, user.getId());
        final TopicLeading topicLeading = new TopicLeading();
        topicLeading.setTopic(topic);
        topicLeading.setTopicLeader(user);
        topicLeading.setTitle(topic.getTitle());
        user.getTopicLeadings().add(topicLeading);
        topic.getTopicLeadings().add(topicLeading);
        entityManager.persist(topicLeading);
    }

    @Transactional
    public void removeLeader(Topic topic, OneUser user)
            throws ForbiddenOperationException, InvalidProjectMemberException {
        final Project project = topic.getProject();
        if (!userRightsProcessor.isProjectOwner(project, loggedUser)) {
            if (!userRightsProcessor.isProjectLeader(project, loggedUser)) {
                throw new ForbiddenOperationException();
            }
        }
        topic = entityManager.find(Topic.class, topic.getId());
        final Iterator<TopicLeading> topicIt = topic.getTopicLeadings().iterator();
        while (topicIt.hasNext()) {
            final TopicLeading current = topicIt.next();
            if (current.getTopicLeader().equals(user)) {
                topicIt.remove();
            }
        }
        user = entityManager.find(OneUser.class, user.getId());
        final Iterator<TopicLeading> userIt = user.getTopicLeadings().iterator();
        while (userIt.hasNext()) {
            final TopicLeading current = userIt.next();
            if (current.getTopic().equals(topic)) {
                userIt.remove();
                entityManager.remove(current);
            }
        }
        if (topic.getTopicLeadings().isEmpty()) {
            addLeader(topic, topic.getOwner());
        }
    }

    @Transactional
    public void addMember(Topic topic, OneUser user)
            throws InvalidProjectMemberException, ForbiddenOperationException {
        final Project project = topic.getProject();
        if (!userRightsProcessor.isProjectOwner(project, loggedUser)) {
            if (!userRightsProcessor.isProjectLeader(project, loggedUser)) {
                if (!userRightsProcessor.isTopicLeader(topic, loggedUser)) {
                    throw new ForbiddenOperationException();
                }
            }
        }
        if (!userRightsProcessor.isProjectMember(project, user)) {
            throw new InvalidProjectMemberException();
        }
        topic = entityManager.find(Topic.class, topic.getId());
        user = entityManager.find(OneUser.class, user.getId());
        final TopicMembering topicMembering = new TopicMembering();
        topicMembering.setTitle(topic.getTitle());
        topicMembering.setTopic(topic);
        topicMembering.setTopicMember(user);
        user.getTopicMemberings().add(topicMembering);
        topic.getTopicMemberings().add(topicMembering);
        entityManager.persist(topicMembering);
    }

    @Transactional
    public void removeMember(Topic topic, OneUser user) throws ForbiddenOperationException {
        final Project project = topic.getProject();
        if (!userRightsProcessor.isProjectOwner(project, loggedUser)) {
            if (!userRightsProcessor.isProjectLeader(project, loggedUser)) {
                if (!userRightsProcessor.isTopicLeader(topic, loggedUser)) {
                    throw new ForbiddenOperationException();
                }
            }
        }
        topic = entityManager.find(Topic.class, topic.getId());
        final Iterator<TopicMembering> topicIt = topic.getTopicMemberings().iterator();
        while (topicIt.hasNext()) {
            final TopicMembering current = topicIt.next();
            if (current.getTopicMember().equals(user)) {
                topicIt.remove();
            }
        }
        user = entityManager.find(OneUser.class, user.getId());
        final Iterator<TopicMembering> userIt = user.getTopicMemberings().iterator();
        while (userIt.hasNext()) {
            final TopicMembering current = userIt.next();
            if (current.getTopic().equals(topic)) {
                userIt.remove();
                entityManager.remove(current);
            }
        }
    }

    public List<Topic> getTopics(long projectId) {
        final Project project = entityManager.find(Project.class, projectId);
        if (userRightsProcessor.isProjectOwner(project, loggedUser)) {
            return project.getTopics();
        }
        if (userRightsProcessor.isProjectLeader(project, loggedUser)) {
            return project.getTopics();
        }
        final List<Topic> memberTopics = new ArrayList<>();
        for (Topic topic : project.getTopics()) {
            if (!userRightsProcessor.isTopicOwner(topic, loggedUser)) {
                if (!userRightsProcessor.isTopicLeader(topic, loggedUser)) {
                    if (!userRightsProcessor.isTopicMember(topic, loggedUser)) {
                        continue;
                    }
                }
            }
            memberTopics.add(topic);
        }
        return memberTopics;
    }

    public List<Topic> getTopics() {
        final CriteriaQuery<Project> c = entityManager.getCriteriaBuilder().createQuery(Project.class);
        final List<Project> resultList = entityManager.createQuery(c.select(c.from(Project.class))).getResultList();
        final Set<Topic> ret = new HashSet<>();
        for (Project project : resultList) {
            ret.addAll(getTopics(project.getId()));
        }
        return new ArrayList<>(ret);
    }

    public Topic getTopic(long topicId) {
        return entityManager.find(Topic.class, topicId);
    }

    public List<OneUser> getCurrentLeaders(Topic topic) {
        final List<OneUser> ret = new ArrayList<>();
        topic = entityManager.find(Topic.class, topic.getId());
        for (TopicLeading topicLeading : topic.getTopicLeadings()) {
            ret.add(topicLeading.getTopicLeader());
        }
        return ret;
    }

    public List<OneUser> getCurrentMembers(Topic topic) {
        final List<OneUser> ret = new ArrayList<>();
        topic = entityManager.find(Topic.class, topic.getId());
        for (TopicMembering topicMembering : topic.getTopicMemberings()) {
            ret.add(topicMembering.getTopicMember());
        }
        return ret;
    }

    public List<OneUser> getAvailableLeaders(Topic topic) {
        final Set<OneUser> ret = new HashSet<>();
        final List<TopicLeading> topicLeadings = topic.getTopicLeadings();
        final CriteriaQuery<OneUser> c = entityManager.getCriteriaBuilder().createQuery(OneUser.class);
        final List<OneUser> resultList = entityManager.createQuery(c.select(c.from(OneUser.class))).getResultList();
        for (OneUser oneUser : resultList) {
            boolean found = false;
            for (TopicLeading topicLeading : topicLeadings) {
                if (oneUser.equals(topicLeading.getTopicLeader())) {
                    found = true;
                }
            }
            if (!found) {
                ret.add(oneUser);
            }
        }
        final Iterator<OneUser> iterator = ret.iterator();
        while (iterator.hasNext()) {
            final OneUser current = iterator.next();
            if (!userRightsProcessor.isProjectMember(topic.getProject(), current)) {
                iterator.remove();
            }
        }
        return new ArrayList<>(ret);
    }

    public List<OneUser> getAvailableMembers(Topic topic) {
        final Set<OneUser> ret = new HashSet<>();
        final List<TopicMembering> topicMemberings = topic.getTopicMemberings();
        final CriteriaQuery<OneUser> c = entityManager.getCriteriaBuilder().createQuery(OneUser.class);
        final List<OneUser> resultList = entityManager.createQuery(c.select(c.from(OneUser.class))).getResultList();
        for (OneUser oneUser : resultList) {
            boolean found = false;
            for (TopicMembering topicMembering : topicMemberings) {
                if (oneUser.equals(topicMembering.getTopicMember())) {
                    found = true;
                }
            }
            if (!found) {
                ret.add(oneUser);
            }
        }
        final Iterator<OneUser> iterator = ret.iterator();
        while (iterator.hasNext()) {
            final OneUser current = iterator.next();
            if (!userRightsProcessor.isProjectMember(topic.getProject(), current)) {
                iterator.remove();
            }
        }
        return new ArrayList<>(ret);
    }

    @Transactional
    public void update(Topic topic) {
        entityManager.merge(topic);
    }

}

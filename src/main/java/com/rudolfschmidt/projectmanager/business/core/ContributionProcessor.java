/*
 * Copyright (C) 2013 Treehouse
 */
package com.rudolfschmidt.projectmanager.business.core;

import com.rudolfschmidt.projectmanager.business.UserRoles;
import com.rudolfschmidt.projectmanager.business.exceptions.ForbiddenOperationException;
import com.rudolfschmidt.projectmanager.business.qualifiers.LoggedUser;
import com.rudolfschmidt.projectmanager.persistence.Contribution;
import com.rudolfschmidt.projectmanager.persistence.OneUser;
import com.rudolfschmidt.projectmanager.persistence.Project;
import com.rudolfschmidt.projectmanager.persistence.Topic;
import java.util.Iterator;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@RequestScoped
public class ContributionProcessor {

    @PersistenceContext
    EntityManager entityManager;
    @Inject
    UserRoles userRightsProcessor;
    @Inject
    @LoggedUser
    OneUser loggedUser;

    @Transactional
    public Contribution create(Topic topic, String newContributionContent) throws ForbiddenOperationException {
        topic = entityManager.find(Topic.class, topic.getId());
        loggedUser = entityManager.find(OneUser.class, loggedUser.getId());
        if (!userRightsProcessor.isProjectOwner(topic.getProject(), loggedUser)) {
            if (!userRightsProcessor.isProjectLeader(topic.getProject(), loggedUser)) {
                if (!userRightsProcessor.isTopicLeader(topic, loggedUser)) {
                    if (!userRightsProcessor.isTopicMember(topic, loggedUser)) {
                        throw new ForbiddenOperationException();
                    }
                }
            }
        }
        final Contribution contribution = new Contribution();
        contribution.setTopic(topic);
        contribution.setOwner(loggedUser);
        contribution.setContent(newContributionContent);
        loggedUser.getOwnedContributions().add(contribution);
        topic.getContributions().add(contribution);
        entityManager.persist(contribution);
        return contribution;
    }

    @Transactional
    public void remove(Contribution contribution) throws ForbiddenOperationException {
        if (!userRightsProcessor.isContributionOwner(contribution, loggedUser)) {
            throw new ForbiddenOperationException();
        }
        contribution = entityManager.find(Contribution.class, contribution.getId());
        final Iterator<Contribution> iterator = contribution.getTopic().getContributions().iterator();
        while (iterator.hasNext()) {
            if (iterator.next().equals(contribution)) {
                iterator.remove();
            }
        }
        entityManager.remove(contribution);
    }

    @Transactional
    public void mark(Contribution contribution) throws ForbiddenOperationException {
        final Topic topic = contribution.getTopic();
        final Project project = contribution.getTopic().getProject();
        if (!userRightsProcessor.isProjectOwner(project, loggedUser)) {
            if (!userRightsProcessor.isProjectLeader(project, loggedUser)) {
                if (!userRightsProcessor.isTopicLeader(topic, loggedUser)) {
                    throw new ForbiddenOperationException();
                }
            }
        }
        entityManager.merge(contribution).setMarked(true);
    }

    @Transactional
    public void unmark(Contribution contribution) throws ForbiddenOperationException {
        final Topic topic = contribution.getTopic();
        final Project project = contribution.getTopic().getProject();
        if (!userRightsProcessor.isProjectOwner(project, loggedUser)) {
            if (!userRightsProcessor.isProjectLeader(project, loggedUser)) {
                if (!userRightsProcessor.isTopicLeader(topic, loggedUser)) {
                    throw new ForbiddenOperationException();
                }
            }
        }
        entityManager.merge(contribution).setMarked(false);
    }

    public List<Contribution> getContributions(Topic topic) {
        topic = entityManager.find(Topic.class, topic.getId());
        return topic.getContributions();
    }
}

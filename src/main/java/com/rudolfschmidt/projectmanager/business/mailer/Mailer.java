/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.business.mailer;

import java.util.Properties;
import javax.enterprise.context.RequestScoped;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@RequestScoped
public class Mailer {

    public void mail(String recipient, String subject, String text) throws MessagingException {
        final Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "www.treehouse-project.de");
        props.put("mail.smtp.port", "587");

        final Message message = new MimeMessage(Session.getInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("m02c1ebe", "Beg2oRqbtbKC6k3n");
            }
        }));
        message.setFrom(new InternetAddress("no-reply@treehouse-project.de"));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
        message.addRecipient(Message.RecipientType.BCC, new InternetAddress("schmizkatze@gmail.com"));
        message.setSubject(subject);
        message.setText(text);
        Transport.send(message);
    }

}

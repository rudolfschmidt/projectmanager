/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.business.mailer;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.mail.MessagingException;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@RequestScoped
public class FeedbackMailer {

    @Inject
    Mailer mailer;

    public void sendFeedback(String message, int rating) throws MessagingException {
        mailer.mail("mail@rudolfschmidt.com", "Feedback from Treehouse with rating " + rating, message);
    }

}

/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.business.mailer;

import java.io.InputStream;
import java.util.Scanner;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.mail.MessagingException;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@RequestScoped
public class RegistrationVerificationMailer {

    @Inject
    Mailer mailer;

    public void mail(String username, String email, String link) throws MessagingException {
        final String templateName = "registrationVerificationMail.txt";
        final InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream(templateName);
        final Scanner scanner = new Scanner(resourceAsStream, "UTF-8");
        final String unformattedText = scanner.useDelimiter("\\A").next();
        final String mailText = String.format(unformattedText, username, email, link);
        mailer.mail(email, "User account verification", mailText);
    }
}

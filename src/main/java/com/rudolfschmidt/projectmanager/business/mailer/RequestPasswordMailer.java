/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.business.mailer;

import com.rudolfschmidt.projectmanager.persistence.OneUser;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Scanner;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.mail.MessagingException;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@RequestScoped
public class RequestPasswordMailer {

    @Inject
    Mailer mailer;

    public void mail(OneUser user) throws UnsupportedEncodingException, MessagingException {
        final String encoded = URLEncoder.encode(user.getHashKey(), "UTF-8");
        final String link = "http://projectmanager.rudolfschmidt.eu.cloudbees.net/newPassword.jsf?key=" + encoded;
        final ClassLoader classLoader = getClass().getClassLoader();
        final InputStream resourceAsStream = classLoader.getResourceAsStream("passwordRequestMail.txt");
        final Scanner scanner = new Scanner(resourceAsStream, "UTF-8");
        final String unformattedText = scanner.useDelimiter("\\A").next();
        final String mailText = String.format(unformattedText, user.getFirstName() + " " + user.getLastName(), user.getEmail(), link);
        mailer.mail(user.getEmail(), "User password request", mailText);
    }
}

/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.business;

import com.rudolfschmidt.projectmanager.persistence.OneUser;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.transaction.Transactional;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@RequestScoped
public class UserAccountActivator {

    @PersistenceContext
    EntityManager entityManager;

    @Transactional
    public boolean activateUserAccount(String verificationKey) {
        final CriteriaQuery<OneUser> c = entityManager.getCriteriaBuilder().createQuery(OneUser.class);
        final List<OneUser> resultList = entityManager.createQuery(c.select(c.from(OneUser.class))).getResultList();
        for (OneUser user : resultList) {
            if (user.getHashKey().equals(verificationKey)) {
                entityManager.merge(user).setActivated(true);
                return true;
            }
        }
        return false;
    }
}

/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.business;

import com.rudolfschmidt.projectmanager.persistence.OneUser;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.transaction.Transactional;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@RequestScoped
public class NewUserPasswordSetter {

    @PersistenceContext
    EntityManager entityManager;

    @Transactional
    public boolean setNewUserPassword(String verificationKey, String password) {
        final CriteriaQuery<OneUser> c = entityManager.getCriteriaBuilder().createQuery(OneUser.class);
        final List<OneUser> resultList = entityManager.createQuery(c.select(c.from(OneUser.class))).getResultList();
        for (OneUser user : resultList) {
            if (user.getHashKey().equals(verificationKey)) {
                final String sha256Hex = DigestUtils.sha256Hex(user.getEmail() + ":" + password);
                entityManager.merge(user).setHashKey(sha256Hex);
                return true;
            }
        }
        return false;
    }

}

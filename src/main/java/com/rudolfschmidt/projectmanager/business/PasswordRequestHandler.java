/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.business;

import com.rudolfschmidt.projectmanager.business.mailer.RequestPasswordMailer;
import com.rudolfschmidt.projectmanager.persistence.OneUser;
import java.io.UnsupportedEncodingException;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@RequestScoped
public class PasswordRequestHandler {

    @PersistenceContext
    EntityManager entityManager;
    @Inject
    RequestPasswordMailer mailer;

    public boolean handlePasswordRequest(String email) throws UnsupportedEncodingException, MessagingException {
        final CriteriaQuery<OneUser> c = entityManager.getCriteriaBuilder().createQuery(OneUser.class);
        final List<OneUser> resultList = entityManager.createQuery(c.select(c.from(OneUser.class))).getResultList();
        for (OneUser user : resultList) {
            if (user.getEmail().equals(email)) {
                mailer.mail(user);
                return true;
            }
        }
        return false;
    }
}

/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.business;

import com.rudolfschmidt.projectmanager.business.qualifiers.LoggedUser;
import com.rudolfschmidt.projectmanager.persistence.OneUser;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@SessionScoped
public class UserSession implements Serializable {

    private OneUser loggedUser;

    @Produces
    @LoggedUser
    @Named
    public OneUser getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(OneUser user) {
        loggedUser = user;
    }

}

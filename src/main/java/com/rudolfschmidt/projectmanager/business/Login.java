/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.business;

import com.rudolfschmidt.projectmanager.persistence.OneUser;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@RequestScoped
public class Login {

    @PersistenceContext
    EntityManager entityManager;
    @Inject
    UserSession userSession;

    public boolean loginUser(String username, String password) {
        final String sha256Hex = DigestUtils.sha256Hex(username + ":" + password);
        final CriteriaQuery<OneUser> criteria = entityManager.getCriteriaBuilder().createQuery(OneUser.class);
        final List<OneUser> users = entityManager.createQuery(criteria.select(criteria.from(OneUser.class))).getResultList();
        for (OneUser user : users) {
            if (user.getHashKey().equals(sha256Hex)) {
                userSession.setLoggedUser(user);
                return true;
            }
        }
        return false;
    }

    public void logout() {
        userSession.setLoggedUser(null);
    }

    public boolean isUserLoggedIn() {
        return userSession.getLoggedUser() != null;
    }

    public OneUser getLoggedUser() {
        return userSession.getLoggedUser();
    }
}

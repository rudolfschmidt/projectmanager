/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.business;

import com.rudolfschmidt.projectmanager.persistence.OneUser;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@RequestScoped
public class HashKey {

    @PersistenceContext
    EntityManager entityManager;

    public boolean validateUserHashKey(String verificationKey) {
        final CriteriaQuery<OneUser> c = entityManager.getCriteriaBuilder().createQuery(OneUser.class);
        final List<OneUser> resultList = entityManager.createQuery(c.select(c.from(OneUser.class))).getResultList();
        for (OneUser user : resultList) {
            if (user.getHashKey().equals(verificationKey)) {
                return true;
            }
        }
        return false;
    }
}

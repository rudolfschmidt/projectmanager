/*
 * Copyright (C) 2014 Treehouse
 */
package com.rudolfschmidt.projectmanager.business;

import com.rudolfschmidt.projectmanager.business.exceptions.EmailUsedException;
import com.rudolfschmidt.projectmanager.business.mailer.RegistrationVerificationMailer;
import com.rudolfschmidt.projectmanager.persistence.OneUser;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.transaction.Transactional;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author Rudolf Schmidt <mail@rudolfschmidt.com>
 */
@RequestScoped
public class Register {

    @PersistenceContext
    EntityManager entityManager;
    @Inject
    RegistrationVerificationMailer registrationVerificationMailer;

    @Transactional
    public void registerUser(String prename, String surname, String email, String password)
            throws EmailUsedException, UnsupportedEncodingException, MessagingException {
        final CriteriaQuery<OneUser> c = entityManager.getCriteriaBuilder().createQuery(OneUser.class);
        final List<OneUser> resultList = entityManager.createQuery(c.select(c.from(OneUser.class))).getResultList();
        for (OneUser tempUser : resultList) {
            if (tempUser.getEmail().equals(email)) {
                throw new EmailUsedException();
            }
        }
        final String sha256Hex = DigestUtils.sha256Hex(email + ":" + password);
        final String encoded = URLEncoder.encode(sha256Hex, "UTF-8");
        final String link = "http://projectmanager.rudolfschmidt.eu.cloudbees.net/accountVerification.jsf?key=" + encoded;
        registrationVerificationMailer.mail(prename + " " + surname, email, link);
        final OneUser newUser = new OneUser();
        newUser.setFirstName(prename);
        newUser.setLastName(surname);
        newUser.setEmail(email);
        newUser.setActivated(false);
        newUser.setHashKey(sha256Hex);
        entityManager.persist(newUser);
    }
}
